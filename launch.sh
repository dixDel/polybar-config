#!/bin/bash
BAR_NAME=main
MONITOR_SEC=DVI-D-0

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Deal with XRandR 1.5+ randomized monitor names
# https://github.com/jaagr/polybar/wiki
MONITOR=$(polybar -m|tail -1|sed -e 's/:.*$//g')

if [[ $1 ]]; then
    #@TODO check arg
    BAR_NAME=$1
    MONITOR=$MONITOR_SEC
fi

# Launch Polybar, using default config location ~/.config/polybar/config
polybar $BAR_NAME &

echo "Polybar launched..."

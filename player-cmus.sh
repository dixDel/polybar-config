#!/bin/sh
# Use filename if no title tag.

CURRENT_TRACK=
FILE_CURRENT=/tmp/cmus-current
FILE_OFFSET=/tmp/cmus-offset
ICON=" "
OFFSET=0
OUTPUT=
LENGTH_DISPLAY=30
LENGTH_OUTPUT=0
TAG_TITLE=

case "$1" in
    --toggle)
        if cmus-remote -Q | grep -q 'status playing'; then
            cmus-remote -u
        else
            cmus-remote -p
        fi
        ;;
    *)
        if cmus-remote -Q | grep -q 'status playing'; then
            ICON=" "
        fi
        ;;
esac
OUTPUT="$(cmus-remote -Q | grep ' artist ' | awk -Fartist\  '{print $NF}')"
OUTPUT="$OUTPUT"' - '
TAG_TITLE=$(cmus-remote -Q | grep 'title' | awk -Ftitle\  '{print $NF}')
OUTPUT="$OUTPUT""$TAG_TITLE"
if [[ $TAG_TITLE == "" ]]; then
    OUTPUT=$(cmus-remote -Q | grep 'file' | awk -F/  '{print $NF}')
fi
echo $OUTPUT > $FILE_CURRENT
LENGTH_OUTPUT=$(printf "%s" "$OUTPUT" | wc -m)

if [[ -f $FILE_CURRENT ]]; then
    CURRENT_TRACK=$(cat $FILE_CURRENT)
    if [[ $OUTPUT != $CURRENT_TRACK ]]; then
        OFFSET=0
        CURRENT_TRACK=$OUTPUT
    elif [[ -f $FILE_OFFSET ]]; then
        OFFSET=$(cat $FILE_OFFSET)
    fi
fi

if (( $LENGTH_OUTPUT > $LENGTH_DISPLAY )); then
    if (( $OFFSET > $LENGTH_OUTPUT )); then
        OFFSET=0
    fi
    OUTPUT="${OUTPUT:$OFFSET:$LENGTH_DISPLAY}"
    # https://ryanstutorials.net/bash-scripting-tutorial/bash-arithmetic.php
    let OFFSET=$OFFSET+$LENGTH_DISPLAY
    echo $OFFSET > $FILE_OFFSET
fi
echo "$ICON""$OUTPUT"

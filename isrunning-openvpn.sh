#!/bin/sh
OUTPUT="%{O-12}  "

if [ "$(pgrep openvpn)" ]; then
    OUTPUT="%{u#859900 +u}$OUTPUT%{F#859900}VPN "
else
    OUTPUT="%{u#dc322f +u}$OUTPUT%{F#dc322f}VPN"
fi

echo "$OUTPUT"

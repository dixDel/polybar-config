#!/bin/bash
lsblk -o mountpoint,fsavail -x mountpoint | awk '$1 ~ /^\/.*$/ { print $1":",$2 }' | sed -rn 's/^(\/[^\/]*)+(\/[^\/:]*)?: (.*)$/%{F#93a1a1}\1:%{F-} \3/p' | tr '\n' ' '

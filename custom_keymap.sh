#!/bin/bash
KEYMAP=$(setxkbmap -query | grep -ie 'layout\|variant' | awk '{print $2}' | tr '\n' ' ')
if [[ $KEYMAP == *"fr bepo"* ]]; then
    echo "bépo"
elif [[ $KEYMAP == *"fr"* ]]; then
    echo "azerty"
elif [[ $KEYMAP == *"ru"* ]]; then
    echo "русский"
else
    echo "other"
fi
